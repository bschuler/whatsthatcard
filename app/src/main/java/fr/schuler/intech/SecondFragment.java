package fr.schuler.intech;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import fr.schuler.intech.model.Autocomplete;
import fr.schuler.intech.model.Cards;
import fr.schuler.intech.model.ImageUris;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SecondFragment extends Fragment {

    Button scryfallCardConverter;
    TextView cardnameText;
    ImageView scryfallArt;
    private static String TAG = "SecondFragmentActivity";
    private String scryfallRandomUrl = "https://api.scryfall.com/cards/random";
    Button answer1;
    Button answer2;
    Button answer3;
    Button answer4;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        scryfallArt = (ImageView) view.findViewById(R.id.scryfallImage);
        scryfallCardConverter = (Button) view.findViewById(R.id.scryfallCardConverter);
        cardnameText = (TextView) view.findViewById(R.id.cardname);
        answer1 = (Button) view.findViewById(R.id.answer1);
        answer2 = (Button) view.findViewById(R.id.answer2);
        answer3 = (Button) view.findViewById(R.id.answer3);
        answer4 = (Button) view.findViewById(R.id.answer4);

        scryfallCardConverter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            OkHttpClient client = new OkHttpClient();
                            Request request = new Request.Builder()
                                    .url(scryfallRandomUrl)
                                    .build();

                            Response response = client.newCall(request).execute();
                            String resp = response.body().string();
                            ObjectMapper objectMapper = new ObjectMapper();
                            Cards myCard = objectMapper.readValue(resp, fr.schuler.intech.model.Cards.class);
                            String cardname = myCard.getName();
                            ImageUris cardUris = myCard.getImageUris();
                            String urlArtCrop = cardUris.getArtCrop();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    cardnameText.setText(myCard.getName());
                                    Picasso.get().load(urlArtCrop).into(scryfallArt);
                                }
                            });
                            List<String> fakeAnswers = findAutocomplete(myCard, client, objectMapper, 4);

                            List<Button> answerButtons = new ArrayList<Button>(){{
                                add(answer1);
                                add(answer2);
                                add(answer3);
                                add(answer4);
                            }};
                            fakeAnswers.add(cardname);
                            Collections.shuffle(fakeAnswers);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (int answer=0; answer<4; answer++){
                                        answerButtons.get(answer).setText(fakeAnswers.get(answer));
                                    }
                                }
                            });

                            response.body().close();
                        }
                        catch (Exception e){
                            e.printStackTrace();
                            Log.i(TAG, "get request failed");
                        }
                    }
                }).start();
            }
        });
        Log.i(TAG, "so far");
    }

    public List<String> findAutocomplete(Cards card, OkHttpClient client, ObjectMapper objectMapper, int precision){
        try{
            String cardname = card.getName();
            String firstCharacters = cardname.substring(0, precision);
            Request request = new Request.Builder()
                    .url("https://api.scryfall.com/cards/autocomplete?q="+firstCharacters)
                    .build();
            Response response = client.newCall(request).execute();
            String resp = response.body().string();
            Autocomplete autocomplete = objectMapper.readValue(resp, fr.schuler.intech.model.Autocomplete.class);
            int dataSize = autocomplete.getTotalValues();
            if (dataSize < 4){
                return findAutocomplete(card, client, objectMapper, precision-1);
            }
            List<String> adjacentNames = autocomplete.getData();
            int containsCardName = adjacentNames.indexOf(cardname);
            if (containsCardName != -1){
                 adjacentNames.remove(containsCardName);
            }
            adjacentNames = pickNRandom(adjacentNames, 3);
            response.body().close();
            return adjacentNames;
        }
        catch (Exception e){
            e.printStackTrace();
            Log.i(TAG, "Autocomplete request failed");
        }
        return null;
    }

    public static List<String> pickNRandom(List<String> lst, int n) {
        List<String> copy = new LinkedList<String>(lst);
        Collections.shuffle(copy);
        return copy.subList(0, n);
    }
}